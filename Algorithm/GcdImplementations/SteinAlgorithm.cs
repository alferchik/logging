﻿using System;
using System.Diagnostics;
using Gcd.v3.Interfaces;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

#pragma warning disable SA1300

namespace Gcd.v3.GcdImplementations
{
    /// <inheritdoc/>
    public class SteinAlgorithm : IAlgorithm
    {
        /// <summary>
        /// Calculates the GCD of integers [-int.MaxValue;int.MaxValue].
        /// </summary>
        /// <param name="first">first value.</param>
        /// <param name="second">second value.</param>
        /// <returns>The GCD value.</returns>
        public int Calculate(int first, int second)
        {
            if (first == int.MinValue || second == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(first), $"Number cannot be {int.MinValue}.");
            }

            if (first == 0 && second == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            first = Math.Abs(first);
            second = Math.Abs(second);

            (first, second) = second > first ? (second, first) : (first, second);

            int gcd = (first, second) switch
            {
                _ when first == 0 && second != 0 => second,
                _ when first != 0 && second == 0 => first,
                _ => Gcd(first, second)
            };

            return gcd;
        }

        /// <summary>
        /// Creates a logger instance.
        /// </summary>
        /// <param name="loggerFactory">Logger Factory.</param>
        /// <param name="loggerName">Logger Name.</param>
        /// <returns>ILogger instance.</returns>
        public ILogger CreateLogger(ILoggerFactory loggerFactory, string loggerName = "Default_name")
        {
            if (loggerFactory is null)
            {
                throw new ArgumentNullException(nameof(loggerFactory), "Logger is null.");
            }
            return loggerFactory.CreateLogger(loggerName);
        }

        /// <summary>
        /// Method to get a calculation time.
        /// </summary>
        /// <param name="first">first value.</param>
        /// <param name="second">second value.</param>
        /// <returns>Time in milliseconds.</returns>
        public (long miliseconds, int gcd) CalculateWithProfiling(int first, int second)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int gcd = this.Calculate(first, second);
            sw.Stop();

            return (sw.ElapsedMilliseconds, gcd);
        }

        private static int Gcd(int first, int second)
        {
            int factor = 0;

            while (first != second)
            {
                if ((first & 1) == 1)
                {
                    second = (second & 1) == 1 ? (first - second) >> 1 : second >> 1;
                    first = (second & 1) == 1 ? first - second : first;
                }
                else
                {
                    if ((second & 1) == 1)
                    {
                        first >>= 1;
                        (first, second) = first < second ? (second, first) : (first, second);
                    }
                    else
                    {
                        first >>= 1;
                        second >>= 1;
                        ++factor;
                    }
                }
            }

            return first << factor;
        }
    }
}
