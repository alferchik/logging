﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Gcd.v3.Interfaces;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

#pragma warning disable SA1300

namespace Gcd.v3.GcdImplementations
{
    /// <summary>
    /// Wrapper class for basic GCD calculation algorythms.
    /// </summary>
    public class GcdWrapper : IAlgorithm
    {
        private readonly IAlgorithm algorithm;

        private ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="GcdWrapper"/> class.
        /// </summary>
        /// <param name="algorithm">PArticular algoruthm for gcd calculation.</param>
        /// <param name="loggerFactory">Logger Factory.</param>
        public GcdWrapper(IAlgorithm algorithm, ILoggerFactory loggerFactory)
        {
            this.algorithm = algorithm ?? throw new ArgumentNullException(nameof(algorithm));
            this.logger = this.CreateLogger(loggerFactory, "Default_logger") ?? throw new ArgumentNullException(nameof(loggerFactory));
            loggerFactory.Dispose();
        }

        /// <summary>
        /// Creates a logger instance.
        /// </summary>
        /// <param name="loggerFactory">Logger Factory.</param>
        /// <param name="loggerName">Logger Name.</param>
        /// <returns>ILogger instance.</returns>
        public ILogger CreateLogger(ILoggerFactory loggerFactory, string loggerName = "Default_name")
        {
            if (loggerFactory is null)
            {
                throw new ArgumentNullException(nameof(loggerFactory), "Logger is null.");
            }

            return loggerFactory.CreateLogger(loggerName);
        }

        /// <summary>
        /// Call of algorithm's calculate method.
        /// </summary>
        /// <param name="first">First number.</param>
        /// <param name="second">Second number.</param>
        /// <returns>GCD.</returns>
        public int Calculate(int first, int second)
        {
            int gcd = 0;

            try
            {
                gcd = this.algorithm.Calculate(first, second);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                this.logger.LogInformation($"{first} first number, {second} second number");
                this.logger.LogError(ex, "At least one of numbers is int.Minvalue.");
                throw;
            }
            catch (ArgumentException ex)
            {
                this.logger.LogInformation($"{first} first number, {second} second number");
                this.logger.LogError(ex, "Both numbers are equal to zero");
                throw;
            }

            return gcd;
        }

        /// <summary>
        /// GCD for three numbers.
        /// </summary>
        /// <param name="first">First number.</param>
        /// <param name="second">Second number.</param>
        /// <param name="third">Third number.</param>
        /// <returns>GCD.</returns>
        public int Calculate(int first, int second, int third)
        {
            if (first == int.MinValue || second == int.MinValue || third == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(first), $"Number cannot be {int.MinValue}.");
            }

            if (first == 0 && second == 0 && third == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            int gcd = 0;

            try
            {
                gcd = (first, second, third) switch
                {
                    (_, 0, 0) => Math.Abs(first),
                    (0, _, 0) => Math.Abs(second),
                    (0, 0, _) => Math.Abs(third),
                    _ => this.algorithm.Calculate(this.algorithm.Calculate(first, second), third)
                };
            }
            catch (ArgumentOutOfRangeException ex)
            {
                this.logger.LogInformation($"{first} first number, {second} second number");
                this.logger.LogError(ex, "At least one of numbers is int.Minvalue.");
                throw;
            }
            catch (ArgumentException ex)
            {
                this.logger.LogInformation($"{first} first number, {second} second number");
                this.logger.LogError(ex, "Both numbers are equal to zero");
                throw;
            }

            return gcd;
        }

        /// <summary>
        /// GCD for array of numbers.
        /// </summary>
        /// <param name="first">First number.</param>
        /// <param name="second">Second number.</param>
        /// <param name="numbers">Array of numbers.</param>
        /// <returns>GCD.</returns>
        public int Calculate(int first, int second, params int[] numbers)
        {
            if (first == int.MinValue || second == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(first), $"Number cannot be {int.MinValue}.");
            }

            if (first == 0 && second == 0 && !ValidParams(numbers))
            {
                throw new ArgumentException($"All numbers cannot be 0 at the same time.");
            }

            int gcd = 0;

            try
            {
                gcd = first == 0 && second == 0 ? 0 : this.algorithm.Calculate(first, second);

                for (int i = 0; i < numbers.Length; i++)
                {
                    if (numbers[i] == 0)
                    {
                        continue;
                    }

                    gcd = this.algorithm.Calculate(gcd, numbers[i]);
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                this.logger.LogInformation($"{first} first number, {second} second number");
                this.logger.LogError(ex, "At least one of numbers is int.Minvalue.");
                throw;
            }
            catch (ArgumentException ex)
            {
                this.logger.LogInformation($"{first} first number, {second} second number");
                this.logger.LogError(ex, "Both numbers are equal to zero");
                throw;
            }

            return gcd;
        }

        /// <summary>
        /// Method to get a gcd and calculation time.
        /// </summary>
        /// <param name="first">first value.</param>
        /// <param name="second">second value.</param>
        /// <returns>Time in milliseconds.</returns>
        public (long miliseconds, int gcd) CalculateWithProfiling(int first, int second)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int gcd = this.Calculate(first, second);
            sw.Stop();

            return (sw.ElapsedMilliseconds, gcd);
        }

        /// <summary>
        /// Method to get a gcd and calculation time.
        /// </summary>
        /// <param name="first">first value.</param>
        /// <param name="second">second value.</param>
        /// <param name="third">third value.</param>
        /// <returns>Time in milliseconds.</returns>
        public (long miliseconds, int gcd) CalculateWithProfiling(int first, int second, int third)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int gcd = this.Calculate(first, second, third);
            sw.Stop();

            return (sw.ElapsedMilliseconds, gcd);
        }

        /// <summary>
        /// Method to get a gcd and calculation time.
        /// </summary>
        /// <param name="first">first value.</param>
        /// <param name="second">second value.</param>
        /// <param name="numbers">numbers array.</param>
        /// <returns>Time in milliseconds.</returns>
        public (long miliseconds, int gcd) CalculateWithProfiling(int first, int second, params int[] numbers)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int gcd = this.Calculate(first, second, numbers);
            sw.Stop();

            return (sw.ElapsedMilliseconds, gcd);
        }

        private static bool ValidParams(int[] numbers)
        {
            int zerosCounter = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == 0)
                {
                    zerosCounter++;
                }
                else if (numbers[i] == int.MinValue)
                {
                    throw new ArgumentOutOfRangeException(nameof(numbers), $"Number cannot be {int.MinValue}.");
                }
            }

            return zerosCounter != numbers.Length;
        }
    }
}
