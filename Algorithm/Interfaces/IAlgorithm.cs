﻿using System;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
#pragma warning disable SA1300

namespace Gcd.v3.Interfaces
{
    /// <summary>
    /// Calculates the GCD of integers [-int.MaxValue;int.MaxValue].
    /// </summary>
    public interface IAlgorithm
    {
        /// <summary>
        /// Calculates the GCD of integers [-int.MaxValue;int.MaxValue].
        /// </summary>
        /// <param name="first">first value.</param>
        /// <param name="second">second value.</param>
        /// <returns>The GCD value.</returns>
        int Calculate(int first, int second);

        /// <summary>
        /// Method for logger creation.
        /// </summary>
        /// <param name="loggerFactory">Logger Factory.</param>
        /// <param name="loggerName">Logger Name.</param>
        /// <returns>Instance of ILogger.</returns>
        ILogger CreateLogger(ILoggerFactory loggerFactory, string loggerName);

        /// <summary>
        /// Method to get a calculation time.
        /// </summary>
        /// <param name="first">first value.</param>
        /// <param name="second">second value.</param>
        /// <returns>Time in milliseconds.</returns>
        (long miliseconds, int gcd) CalculateWithProfiling(int first, int second);
    }
}
